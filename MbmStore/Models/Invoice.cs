﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MbmStore.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations.Schema;

namespace MbmStore.Models
{
   
    
    public class Invoice
    {
        // fields
        private decimal totalPrice;

        // props
        public int InvoiceId { get; set; }
        public DateTime OrderDate { get; set; }

        //public decimal TotalPrice { get; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        public List<OrderItem> OrderItems { get; } = new List<OrderItem>();

       
        public Invoice() { 
        
        }
        public Invoice(int invoiceId, DateTime orderDate, Customer customer) {
            InvoiceId = invoiceId;
            OrderDate = orderDate;
            Customer = customer;
        }

        public void AddOrderItem(Product product, int quantity) {

            OrderItem item = new OrderItem(product, quantity);
            OrderItems.Add(item);

        }

        public decimal GetTotalPrice() {

            decimal totalPrice = 0M;

            foreach (OrderItem item in OrderItems)
            {
                totalPrice += item.TotalPrice;
            }

            return totalPrice;

        }
    }
}
