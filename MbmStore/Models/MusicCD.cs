﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MbmStore.Models
{
    public class MusicCD : Product
    {

        //private readonly List<Track> tracks = new List<Track>();

        public string Artist { get; set; }
        public string Label { get; set; }
        public short Released { get; set; }
        public List<Track> Tracks { get; } = new List<Track>();

        public MusicCD()
        {

        }
        public MusicCD(int productId,string artist, string title, decimal price, short released): base(productId,title, price)
        {
            Artist = artist;
            Released = released;

        }

        public void AddTrack(Track track) {

            Tracks.Add(track);
        
        }

        public TimeSpan TotalPlayingTime() {

            TimeSpan total = new TimeSpan();

            foreach (Track track in Tracks) {

                total += track.Length;
            }

            return total;


        }
    }
}
