﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MbmStore.Models;

namespace MbmStore.Models
{
    public class OrderItem
    {

        public int OrderItemId { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public decimal TotalPrice { get; }
        public int InvoiceId { get; set; }
        public OrderItem() { 
        
        }
        public OrderItem(Product product, int quantity)
        {
            this.Product = product;
            this.Quantity = quantity;

            if (product!=null)
            this.TotalPrice += this.Product.Price * this.Quantity;
        }

       
    }
}
