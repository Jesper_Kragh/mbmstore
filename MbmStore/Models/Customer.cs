﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace MbmStore.Models
{
    public class Customer
    {

        //props
        public int CustomerId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
    // public List<string> PhoneNumbers { get; } = new List<string>();

    [Column(TypeName = "datetime2")]

        public DateTime Birthdate { get; set; }
       
        
        public int Age  { 
            get {
                DateTime now = DateTime.Now;
                int age = now.Year - Birthdate.Year; if (now.Month < Birthdate.Month || (now.Month == Birthdate.Month && now.Day < Birthdate.Day)) { age--; }
                return age;
            }
        }

        public virtual ICollection<Invoice> Invoices { get; set; }
        public virtual ICollection<Phone> PhoneNumbers { get; set; } = new List<Phone>();
        // constructor
        public Customer() {}

        public Customer(int customerId, string firstName, string lastName, string address, string zip,string city){
            this.CustomerId = customerId;
            this.Firstname = firstName;
            this.Lastname = lastName;
            this.Address = address;
            this.Zip = zip;
            this.City = city;
        }

        public void AddPhoneNumber(Phone phone) {
            PhoneNumbers.Add(phone);
        }

        public void AddInvoice(Invoice invoice) {
            Invoices.Add(invoice);
        }


    }
}
