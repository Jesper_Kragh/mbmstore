﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Threading.Tasks;
using MbmStore.Models;

namespace MbmStore.Infrastructure
{
    public static class Repository
    {

        public static List<Product> Products { get; } = new List<Product>();
        public static List<Invoice> Invoices { get; } = new List<Invoice>();

        static Repository() {

            // BOOKS

            Book book1 = new Book(1,"S. King", "IT", 180.00M, 1986);
            book1.Publisher = "Publishing Company";
            book1.ISBN = "0316547832";
            book1.ImageFileName = "book1.jpg";
            book1.Category = "Book";
            Products.Add(book1);

            Book book2 = new Book(2,"S. King", "Carrie", 240.50M, 1998);
            book2.Publisher = "Publishing Company II";
            book2.ISBN = "0316547832";
            book2.ImageFileName = "book2.jpg";
            book2.Category = "Book";
            Products.Add(book2);

            Book book3 = new Book(3,"F. Dostojevski", "Onde Ånder", 90.95M, 1998);
            book3.Publisher = "Publishing Company";
            book3.ISBN = "0316547832";
            book3.ImageFileName = "book3.jpg";
            book3.Category = "Book";
            Products.Add(book3);

            // MUSIC

            MusicCD cd1 = new MusicCD(4,"The Sleeplings", "For Dare And Farewell", 120.00M, 2007);
            cd1.Label = "Marrowphone Recordings";
            cd1.Category = "Music";
            cd1.ImageFileName = "cd1.jpg";
            cd1.AddTrack(new Track("Vehicle", "J. K. Sleepling", new TimeSpan(0,3,36)));
            cd1.AddTrack(new Track("A Murder of Crows", "J. K. Sleepling", new TimeSpan(0, 4, 19)));
            cd1.AddTrack(new Track("Away", "J. K. Sleepling", new TimeSpan(0, 4, 29)));
            cd1.AddTrack(new Track("Something in the Sky", "J. K. Sleepling", new TimeSpan(0, 4, 22)));
            cd1.AddTrack(new Track("The Still Kids", "J. K. Sleepling", new TimeSpan(0, 3, 57)));
            cd1.AddTrack(new Track("Weasel Bite", "J. K. Sleepling", new TimeSpan(0, 5, 18)));
            Products.Add(cd1);

            MusicCD cd2 = new MusicCD(5,"The Sleeplings", "Elusive Lights of the Long-Forgotten", 120.00M, 2017);
            cd2.Label = "Marrowphone Recordings";
            cd2.ImageFileName = "cd2.jpg";
            cd2.Category = "Music";
            cd2.AddTrack(new Track("Dead Horse", "J. K. Sleepling", new TimeSpan(0, 4, 18)));
            cd2.AddTrack(new Track("Apothecary", "J. K. Sleepling", new TimeSpan(0, 3, 50)));
            cd2.AddTrack(new Track("Faye Valley Skeleton", "J. K. Sleepling", new TimeSpan(0, 4, 03)));
            cd2.AddTrack(new Track("Mary the Quiet", "J. K. Sleepling", new TimeSpan(0, 4, 48)));
            cd2.AddTrack(new Track("Fog Walkers", "J. K. Sleepling", new TimeSpan(0, 3, 49)));
            cd2.AddTrack(new Track("Broken Light Spectre", "J. K. Sleepling", new TimeSpan(0, 4, 49)));
            cd2.AddTrack(new Track("James", "J. K. Sleepling", new TimeSpan(0, 3, 26)));
            cd2.AddTrack(new Track("Long-forgotten", "J. K. Sleepling", new TimeSpan(0, 5, 45)));
            Products.Add(cd2);

            MusicCD cd3 = new MusicCD(6,"The Sleeplings", "Light Leaks", 120.00M, 2020);
            cd3.Label = "Marrowphone Recordings";
            cd3.ImageFileName = "cd3.png";
            cd3.Category = "Music";
            cd3.AddTrack(new Track("Light Leaks", "J. K. Sleepling", new TimeSpan(0, 4, 07)));
            cd3.AddTrack(new Track("All Hallows Eve", "J. K. Sleepling", new TimeSpan(0, 3, 14)));
            cd3.AddTrack(new Track("River", "J. K. Sleepling", new TimeSpan(0, 4, 21)));
            cd3.AddTrack(new Track("Four Hens Down", "J. K. Sleepling", new TimeSpan(0, 4, 02)));
            Products.Add(cd3);

            // MOVIES

            Movie mov1 = new Movie(7,"Sleepy Hollow",  120.00M, "mov1.jpg", "Tim Burton");
            mov1.Category = "Movie";
            Products.Add(mov1);

            Movie mov2 = new Movie(8,"Edward Scissorhands", 120.00M, "mov2.jpg", "Tim Burton");
            mov2.Category = "Movie";
            Products.Add(mov2);

            Movie mov3 = new Movie(9,"Beeteljuice", 120.00M, "mov3.jpg", "Tim Burton");
            mov3.Category = "Movie";
            Products.Add(mov3);



            // CUSTOMERS

            Customer cust1 = new Customer(1, "Ole", "Olsen", "Kirsebærvej 21", "6600", "Vejen");
            cust1.AddPhoneNumber(new Phone(1, "1218 2419", 1, "mobile"));

            Customer cust2 = new Customer(2, "Marie", "Jensen", "Hovvejen 16", "8471", "Sabro");
            cust2.AddPhoneNumber(new Phone(1, "1218 2416", 1, "mobile"));


            // INVOICES

            Invoice inv1 = new Invoice(1,new DateTime(), cust1);
            inv1.AddOrderItem(cd3, 1);
            inv1.AddOrderItem(book1, 1);

            Invoice inv2 = new Invoice(1, new DateTime(), cust2);
            inv2.AddOrderItem(cd2, 1);
            inv2.AddOrderItem(cd1, 1);
            inv2.AddOrderItem(book3, 1);
            inv2.AddOrderItem(mov2, 1);

            Invoices.Add(inv1);
            Invoices.Add(inv2);

        }
    }
}
