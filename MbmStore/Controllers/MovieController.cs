﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MbmStore.Models;
using Microsoft.AspNetCore.Mvc;

namespace MbmStore.Controllers
{
    public class MovieController : Controller
    {
        public IActionResult Index()
        {

            // create a new Movie object with instance name jungleBook
            var jungleBook = new Movie(34,"Jungle Book", 160.50m, "junglebook.jpg","");
            var wizardBook = new Movie(35,"The Wizard of Oz", 230.75m, "twoo.jpg","");
            var aliceBook = new Movie(67,"Alice in Wonderland", 130.50m, "junglebook.jpg","");

            List<object> books = new List<object>();
            books.Add(jungleBook);
            books.Add(wizardBook);
            books.Add(aliceBook);

            // assign a ViewBag property to the new Movie object
            ViewBag.Books = books;

            // return the default view
            return View();
        }
    }
}