﻿using System;
using System.Collections.Generic;
using System.Linq;
using MbmStore.Models;
using MbmStore.Models.ViewModels;
using MbmStore.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using MbmStore.Data;

namespace MbmStore.Controllers
{
    public class CatalogueController : Controller
    {
        private MbmStoreContext dataContext;
        private int PageSize { get; } = 4;

        public CatalogueController(MbmStoreContext dbContext) { 
            dataContext = dbContext; 
        }

        public IActionResult Index(string category, int page = 1)
        {



            ProductsListViewModel model = new ProductsListViewModel
            {
                Products = dataContext.Products
                .Where(p => category == null || p.Category == category)
                .OrderBy(p => p.ProductId)
                .Skip((page - 1) * PageSize)
                .Take(PageSize),

                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = category == null ? dataContext.Products.Count() : dataContext.Products.Where(e => e.Category == category).Count()
                },

                CurrentCategory = category
            };
            return View(model);
        }
    }
}
